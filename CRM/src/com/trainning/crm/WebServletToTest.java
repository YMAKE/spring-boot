package com.trainning.crm;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WebServletToTest
 */
@WebServlet("/WebServletToTest")
public class WebServletToTest extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public WebServletToTest() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uName="hbstudent";
		String password="hbstudent";
		
		PrintWriter out=response.getWriter();
		out.print("Testing DB Connection");
		String Url="jdbc:mysql://localhost:3306/web_customer_tracker";
		String driverCls="com.mysql.jdbc.Driver";
		try {
			Class.forName(driverCls);
			Connection myCon=DriverManager.getConnection(Url, uName, password);
			out.print("DB Connection Success!");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
}
