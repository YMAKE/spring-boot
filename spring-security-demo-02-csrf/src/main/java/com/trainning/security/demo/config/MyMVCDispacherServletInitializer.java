package com.trainning.security.demo.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
/**
 * mapping the web.xml file to anotaed class.
 * @author NIRANJAN
 *
 */
public class MyMVCDispacherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {

		return new Class[] { DemoConfigApp.class };
	}

	@Override
	protected String[] getServletMappings() {

		return new String[] { "/" };
	}

}
