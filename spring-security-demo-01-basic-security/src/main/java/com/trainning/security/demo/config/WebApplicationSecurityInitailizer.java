package com.trainning.security.demo.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

//enable the security filter b/w app and db.
public class WebApplicationSecurityInitailizer extends AbstractSecurityWebApplicationInitializer {

}
