package com.trainning.security.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;

@Configuration
@EnableWebSecurity
public class WebApplicationSecurity extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		UserBuilder users=User.withDefaultPasswordEncoder();
		 
		auth.inMemoryAuthentication()
		.withUser(users.username("niranjan").password("test123").roles("ADMIN"))
		.withUser(users.username("santosh").password("test123").roles("ADMIN"))
		.withUser(users.username("praveen").password("test123").roles("ADMIN"));
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		     
		http.authorizeRequests().anyRequest().authenticated()
		.and().formLogin().loginPage("/showLoginPage").loginProcessingUrl("/authenticateTheUser").permitAll()
		.and().logout().permitAll();
	}
	
	

}
